// In this question, please create a project by yourself with the corresponding test file(s) included

public class QB2_1 {

    public static String toString(int p, int q) {
        
        String s;

        if(p>3 && q<25){
            if(p>50){
               s = "p/q=" + (p/q) ;
            }else{
                s = "p+q="+(p+q);
            }
        
            if(q<12){
                s = "p*q="+(p*q);
            }else{
                s = "sqrt(p*q)="+ Math.sqrt(p*q);
            }
       }else{
            s = "p-q="+(p-q);
       }

       return s;
    }
}