public interface Score {
    /*
    * This Score interface define the method to check student's grade according to the final score against 
    * ANU official grading scale.
    * 
    * The Score1 to Score5 are five different implementations to achieve checkGrade.
    * Only one of them is the correct implementation.
    * Please update the test() method in ScoreTest class to implement your own test.
    * You task is to identify the correct implementation by your test.
    *
    * @param int score: an int value which represents the final grade for this course.
    * @return  String grade: a grade which represent in one of the following String
    *           {"N","P","CR","D","HD"}
    *           N: Fail 0-49
    *           P: Pass 50-59
    *           CR: Credit 60-69
    *           D: Distinction 70-79
    *           HD: High Distinction 80-100
    *           Refer to ANU official grading scale website:
    *           http://www.anu.edu.au/students/program-administration/assessments-exams/grading-scale
    *
    * @Author Lei Huang u6041747@anu.edu.au
    * */

    String grade(int score);
}
