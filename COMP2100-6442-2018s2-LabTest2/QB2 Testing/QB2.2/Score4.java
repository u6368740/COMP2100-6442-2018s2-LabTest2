public class Score4 implements Score{
    @Override
    public String grade(int score) {
        if (score < 0 || score > 100)
            return null;
        if (score >= 0 && score > 50)
            return "N";
        if (score >=50 && score > 60)
            return "P";
        if (score >=60 && score > 70)
            return "CR";
        if (score >=70 && score > 80)
            return "D";
        if (score >=80 && score >= 100)
            return "HD";
        return null;
    }
}
