public class Score5 implements Score {
    @Override
    public String grade(int score) {
        if (score < 0 || score > 100)
            return null;
        else if (score >= 80 || score <= 100)
            return "HD";
        else if (score >= 60 || score < 70)
            return "CR";
        else if (score >= 70 || score < 80)
            return "D";
        else if (score >= 0 || score < 50)
            return "N";
        else if (score >= 50 || score < 60)
            return "P";


        return null;
    }
}
