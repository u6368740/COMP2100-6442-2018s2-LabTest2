public class Demo {
    static void test1() {
        //    0
        //  1   2
        TreeNode root = new TreeNode(0);
        TreeNode node1 = root.setLeft(1);
        TreeNode node2 = root.setRight(2);

        System.out.println("\nTest case 1: \n" + TreeSum.treeSum(root));
        // 3 expected
    }

    static void test2() {
        //       0
        //    1     2
        //   3 4   5 6
        //      7 8
        TreeNode root = new TreeNode(0);
        TreeNode node1 = root.setLeft(1);
        TreeNode node2 = root.setRight(2);
        TreeNode node3 = node1.setLeft(3);
        TreeNode node4 = node1.setRight(4);
        TreeNode node5 = node2.setLeft(5);
        TreeNode node6 = node2.setRight(6);
        TreeNode node7 = node4.setRight(7);
        TreeNode node8 = node5.setLeft(8);

        System.out.println("\nTest case 2: \n" + TreeSum.treeSum(root));
        // 36 expected
    }

    public static void main(String[] args) {
        test1();
        test2();
    }
}
