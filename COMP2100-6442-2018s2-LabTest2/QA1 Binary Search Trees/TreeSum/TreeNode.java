/*
 * This is the implementation of TreeNode.
 *
 * Please read through, but do not modify the codes here.
 *
 */

public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    public TreeNode(int x) {
        val = x;
        left = null;
        right = null;
    }


    // the following are for building demo
    // do not call the following methods in your code
    public void set(int x) {
        val = x;
        return;
    }
    public TreeNode setLeft(int x) {
        if (left == null) {
            left = new TreeNode(x);
        } else {
            left.set(x);
        }
        return left;
    }
    public TreeNode setRight(int x) {
        if (right == null) {
            right = new TreeNode(x);
        } else {
            right.set(x);
        }
        return right;
    }
}
