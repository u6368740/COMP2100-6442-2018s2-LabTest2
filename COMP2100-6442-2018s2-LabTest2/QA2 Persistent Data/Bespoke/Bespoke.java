import java.io.*;
import java.util.ArrayList;

public class Bespoke {

	final static String FILE_NAME = "animals.bespoke";
	static ArrayList<Animal> data;

	public static class Animal {
		String name;
		String breed;
		int age;

		public Animal(String name, String breed, int age) {
			this.name = name;
			this.breed = breed;
			this.age = age;
		}

		@Override
		public String toString() {
			return name + " is a " + age + " year old " + breed;
		}
	}

	static ArrayList<Animal> load(String fileName) {
		ArrayList<Animal> rtn = new ArrayList<Animal>();

		// TODO: Fix this method

		return rtn;

	}

	public static void main(String[] args) {
		data = load(FILE_NAME);
		for (Animal a : data) System.out.println(a);
	}

}