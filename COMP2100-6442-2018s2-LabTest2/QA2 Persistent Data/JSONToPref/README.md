# Description
This problem is about loading a list of shoe sizes from a JSON file (`shoesizes.json`), and 
save it to Java Preferences System.

The JSON loader requires importing `json-simple-1.1.1.jar` to the project build path.

# How to run
1.	Start Eclipse
1.	On the top navigation panel, click "File", then "Import"
1.	Choose "Existing Projects into Workspace", click "Next"
1.	Browse to this folder, and click "Finish".
