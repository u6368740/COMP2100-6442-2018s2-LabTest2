package problem;

import java.util.List;

/**
 * NOTE: this class implementation should not be changed.
 */
public class ShoeSizeJsonToPref {
	private static final String JSON_FILE_NAME = "shoesizes.json";

	public static void main(String[] args) {
		final List<Integer> shoeSizesFromJson = ShoeSizeJson.load(JSON_FILE_NAME);
		System.out.println("Loaded from '" + JSON_FILE_NAME + "': " + shoeSizesFromJson);

		ShoeSizePref.save(shoeSizesFromJson);
		final List<Integer> shoeSizesFromPref = ShoeSizePref.load();
		System.out.println("Saved to Java Pref: " + shoeSizesFromPref);
	}
}
