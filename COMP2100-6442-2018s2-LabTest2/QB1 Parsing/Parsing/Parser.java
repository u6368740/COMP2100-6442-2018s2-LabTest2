import java.util.ArrayList;

public class Parser {

	/*
	grammar

	<exp> ::= <variable>|(<exp>+<exp>)|(<exp>-<exp>)|(<exp>*<exp>)|(<exp>/<exp>)|!(<exp>)|Max(<exps>)
	<exps> ::= <exp>|<exp>,<exps>
	 */

	static Expression parseExp(Tokenizer tok) {
		Token cur = tok.current();

		if (cur.type == TokenType.LBRA) {
			tok.next();
			Expression exp1 = parseExp(tok);
			Token operator = tok.current();

			tok.next();
			Expression exp2 = parseExp(tok);
			cur = tok.current();
			if (cur.type != TokenType.RBRA) throw new Error("RBRA expected");
			tok.next();

			// TODO: parse "(<exp>+<exp>)", "(<exp>-<exp>)", "(<exp>*<exp>)", "(<exp>/<exp>)" (Add, Sub, Multi, Div)




		} else if (cur.type == TokenType.MINUS) {
			tok.next();
			Expression exp = parseExp(tok);
			return new Minus(exp);
		} else if (cur.type == TokenType.INTLIT) {
			int value = cur.value;
			tok.next();
			return new Lit(value);
		} else if (cur.type == TokenType.MAX){


			//TODO: parse "Max(<exps>)"



		} else if (cur.type == TokenType.FAC){


			// TODO: parse "!(<exp>)"



		}
		else {
			throw new Error("unknown expression"); }
		throw new Error("unknown expression");
	}


	static ArrayList<Expression> parseExps(Tokenizer tok,ArrayList exps){


		// TODO: parse "<Exps>"
		return null;


	}

}
