
public abstract class BinExpression extends Expression {

	Expression e1, e2;
	
	public BinExpression (Expression e1, Expression e2) {
		this.e1 = e1;
		this.e2 = e2;
	}
	
	@Override
	public String show() {
		return "(" + e1.show() + symbol() + e2.show() + ")";
	}

	@Override
	public int evaluate() {
		return eval(e1.evaluate(), e2.evaluate());
	}

	public abstract String symbol();
	public abstract int eval(int v1, int v2);
	
}
