
public class Minus extends Expression {

	Expression e;
	
	public Minus(Expression e) {
		this.e = e;
	}
	
	@Override
	public String show() {
		return "( - " + e.show() + ")";
	}

	@Override
	public int evaluate() {
		return - e.evaluate();
	}

}
