public class Comma extends Expression {
    @Override
    public String show() {
        return ",";
    }

    @Override
    public int evaluate() {
        return 0;
    }
}
