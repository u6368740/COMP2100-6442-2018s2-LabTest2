public class Fac extends Expression {

    Expression e;

    public Fac(Expression e){
        this.e = e;
    }

    @Override
    public String show() {
        return "!("+e.show()+")";
    }

    @Override
    public int evaluate() {
        int num = e.evaluate();
        int total = 1;
        if (num == 0){
            return 0;
        }else if (num > 0){
            while (num > 0){
                total *= num;
                num --;
            }
            return total;
        }else {
            throw new Error("negative number");
        }
    }
}