import java.util.ArrayList;

public class Max extends Expression {

    ArrayList<Expression> exps = new ArrayList<>();

    public Max(ArrayList<Expression> exps){
        this.exps = exps;
    }

    @Override
    public String show() {
        String str = "";
        for (Expression e: exps){
            str += e.show()+", ";
        }
        str = str.substring(0, str.length()-2);
        return "Max(" + str + ")";
    }

    @Override
    public int evaluate() {
        int max = exps.get(0).evaluate();
        for (Expression e: exps){
            if (e.evaluate() > max){
                max = e.evaluate();
            }
        }
        return max;
    }
}
