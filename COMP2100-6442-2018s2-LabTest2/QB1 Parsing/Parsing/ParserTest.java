import org.junit.Assert;
import org.junit.Test;

import java.io.IOError;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ParserTest {
    @Test
    public void AddTest() {
        String str = "(   3+6)";
        Tokenizer tok = new MathTokenizer(str);
        Expression e = Parser.parseExp(tok);
        assertTrue("The expression should be (3+6), however provided " + e.show(), e.show().equals("(3+6)"));
        assertTrue("The expression evaluates to 9, however provided " + e.evaluate(), e.evaluate() == 9);


    }

    @Test
    public void SubTest() {
        String str = "(  8-   2)";
        Tokenizer tok = new MathTokenizer(str);
        Expression e = Parser.parseExp(tok);
        assertTrue("The expression should be (8-2), however provided " + e.show(), e.show().equals("(8-2)"));
        assertTrue("The expression evaluates to 6, however provided " + e.evaluate(), e.evaluate() == 6);

    }

    @Test
    public void MultTest() {
        String str = "(( 3+2)*   2)";
        Tokenizer tok = new MathTokenizer(str);
        Expression e = Parser.parseExp(tok);
        assertTrue("The expression should be ((3+2)*2), however provided " + e.show(), e.show().equals("((3+2)*2)"));
        assertTrue("The expression evaluates to 10, however provided " + e.evaluate(), e.evaluate() == 10);


    }

    @Test
    public void DivTest() {
        String str = "((5  -1)/   (1+1))";
        Tokenizer tok = new MathTokenizer(str);
        Expression e = Parser.parseExp(tok);
        assertTrue("The expression should be ((5-1)/(1+1)), however provided " + e.show(), e.show().equals("((5-1)/(1+1))"));
        assertTrue("The expression evaluates to 2, however provided " + e.evaluate(), e.evaluate() == 2);

    }

    @Test
    public void FactorialTest() {
        String str = "!(5)";
        Tokenizer tok = new MathTokenizer(str);
        Expression e = Parser.parseExp(tok);
        assertTrue("The expression should be !(5), however provided " + e.show(), e.show().equals("!(5)"));
        assertTrue("The expression evaluates to 120, however provided " + e.evaluate(), e.evaluate() == 120);


        String str1 = "!((3+ 2))";
        Tokenizer tok1 = new MathTokenizer(str1);
        Expression e1 = Parser.parseExp(tok1);
        assertTrue("The expression should be !((3+2)), however provided " + e1.show(), e1.show().equals("!((3+2))"));
        assertTrue("The expression evaluates to 120, however provided " + e1.evaluate(), e1.evaluate() == 120);


        String str2 = "!(!(3))";
        Tokenizer tok2 = new MathTokenizer(str2);
        Expression e2 = Parser.parseExp(tok2);
        assertTrue("The expression should be !(!(3)), however provided " + e2.show(), e2.show().equals("!(!(3))"));
        assertTrue("The expression evaluates to 720, however provided " + e2.evaluate(), e2.evaluate() == 720);

    }

    @Test
    public void MaxTest() {
        String str = "Max(3  )";
        Tokenizer tok = new MathTokenizer(str);
        Expression e = Parser.parseExp(tok);
        assertTrue("The expression should be Max(3), however provided " + e.show(), e.show().equals("Max(3)"));
        assertTrue("The expression evaluates to 3, however provided " + e.evaluate(), e.evaluate() == 3);


        String str1 = "Max(3, ((3+2) *2), (( 5-1) /1))";
        Tokenizer tok1 = new MathTokenizer(str1);
        Expression e1 = Parser.parseExp(tok1);
        assertTrue("The expression should be Max(3, ((3+2)*2), ((5-1)/1)), however provided " + e1.show(), e1.show().equals("Max(3, ((3+2)*2), ((5-1)/1))"));
        assertTrue("The expression evaluates to 10, however provided " + e1.evaluate(), e1.evaluate() == 10);


    }


    @Test
    public void difficultTest() {
        String str = "(!(6)+Max(  !(3), ((3+2) *2), (( 5-1) /1)))";
        Tokenizer tok = new MathTokenizer(str);
        Expression e = Parser.parseExp(tok);

        assertTrue("The expression should be (!(6)+Max(!(3), ((3+2)*2), ((5-1)/1))), however provided " + e.show(), e.show().equals("(!(6)+Max(!(3), ((3+2)*2), ((5-1)/1)))"));
        assertTrue("The expression evaluates to 730, however provided " + e.evaluate(), e.evaluate() == 730);




    }
}