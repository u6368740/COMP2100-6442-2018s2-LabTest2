
public class Mult extends BinExpression {

	public Mult(Expression e1, Expression e2) {
		super(e1,e2);
	}
	
	@Override
	public String symbol() {
		return "*";
	}

	@Override
	public int eval(int v1, int v2) {
		return v1 * v2;
	}

}
