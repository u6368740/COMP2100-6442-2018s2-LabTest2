public class Lit extends Expression {
	int v;
	public Lit(int v) {
		this.v = v;
	}

	@Override
	public String show() {
		if (v < 0)
			return "(" + v + ")";
		else
			return "" + v;
	}

	@Override
	public int evaluate() {
		return v;
	}
}
